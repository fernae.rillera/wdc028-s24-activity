import React from 'react';
// import Banner from './components/Banner';
import Highlights from '../components/Highlights';
import Course from '../components/Course';
import { Container } from 'react-bootstrap';

export default function Home(){
	return(
		<Container>
			<Highlights />
			<Course />
		</Container>
		)
}