import React from 'react';
import courseData from '../data/coursesdata';
import CourseCard from '../components/CourseCard';


export default function CoursesPage(){
	const courses = courseData.map(indivCourse =>{
		return (
			<CourseCard 
				key={indivCourse.id} 
				courseProp={indivCourse}
			/>
		);
	})

	return (
		<React.Fragment>
			{courses}
		</React.Fragment>

	)
}

