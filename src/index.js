import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
// import NavBar from './NavBar';

import 'bootstrap/dist/css/bootstrap.min.css';

// Process for creating and rendering components
// 1. Create a component
// 2. Import the component
// 3. Render/call the component

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);



